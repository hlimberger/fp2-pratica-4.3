/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Henrique Kolling Limberger
 */
public interface Figura_1 {
    String getNome();
    double getPerimetro();
    double getArea();
}
