/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author henrique
 */
public class TrianguloEquilatero extends Retangulo{
    
    public TrianguloEquilatero(double lado){
        super(lado, lado * Math.sqrt(3) / 2);
    }
    
    public double getBase(){
        return this.getLadoMaior();
    }
    
    public double getAltura(){
        return this.getLadoMenor();
    }

    @Override
    public double getArea(){
        return super.getArea() / 2;
    }
    
    @Override
    public double getPerimetro(){
        return this.getBase() * 3;
    }
    
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
}
