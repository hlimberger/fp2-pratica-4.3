/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Henrique Kolling Limberger
 */
public class Elipse implements FiguraComEixos {
    private final double eixoMenor;
    private final double eixoMaior;
    
    public Elipse(double semiEixoMenor, double semiEixoMaior){
        this.eixoMenor = semiEixoMenor * 2;
        this.eixoMaior = semiEixoMaior * 2;
    }
    
    @Override
    public double getArea(){
        double x = eixoMaior / 2;
        double y = eixoMenor / 2;
        return Math.PI * x * y;
    }
    
    @Override
    public double getPerimetro(){
        double x = eixoMaior / 2;
        double y = eixoMenor / 2;
        return Math.PI * ((3 * (x + y)) - Math.sqrt(((3 * x) + y) * (x + (3 * y))));
    }
    
    @Override
    public double getEixoMenor(){
        return eixoMenor;
    }
    
    @Override
    public double getEixoMaior(){
        return eixoMaior;
    }
    
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
}
