/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Henrique Kolling Limberger
 */
public class Circulo extends Elipse {
    
    public Circulo(double raio){
        super(raio,raio);
    }
    @Override
    public double getArea(){
        return Math.PI * Math.pow(getEixoMaior()/2, 2);
    }
    @Override
    public double getPerimetro(){
        return 2 * Math.PI * this.getRaio();
    }
    
    public double getRaio(){
        return this.getEixoMaior()/2;
    }
    
}
