/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author henrique
 */
public class Retangulo implements FiguraComLados {
    private final double ladoMaior;
    private final double ladoMenor;
    
    public Retangulo(double ladoMaior, double ladoMenor){
        this.ladoMaior = ladoMaior;
        this.ladoMenor = ladoMenor;
    }
    
    @Override
    public double getArea(){
        return this.ladoMaior * this.ladoMenor;
    }
    
    @Override
    public double getPerimetro(){
        return this.ladoMaior * 2 + this.ladoMenor * 2;
    }
    
    @Override
    public double getLadoMaior(){
        return ladoMaior;
    }
    
    @Override
    public double getLadoMenor(){
        return ladoMenor;
    }
    
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
}
