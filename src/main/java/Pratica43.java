/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import utfpr.ct.dainf.if62c.pratica.*;
/**
 *
 * @author henrique
 */
public class Pratica43 {
    public static void main(String[] args){        
        
        Elipse e = new Elipse(4.2, 2.1);
        System.out.println(e.getNome() + " " + e.getEixoMaior()+ " x " + e.getEixoMenor() + " - a área é " + e.getArea() + " e o perímetro é " + e.getPerimetro());
        Elipse e2 = new Elipse(5.7, 5.7);
        System.out.println(e2.getNome() + " " + e2.getEixoMaior()+ " x " + e2.getEixoMenor() + " - a área é " + e2.getArea() + " e o perímetro é " + e2.getPerimetro());
        Circulo c = new Circulo(5.7);
        System.out.println(c.getNome() + " de raio " + c.getEixoMaior()+ " x " + c.getEixoMenor() + " - a área é " + c.getArea() + " e o perímetro é " + c.getPerimetro());
        
        Retangulo r = new Retangulo(4.2, 2.1);
        System.out.println(r.getNome() + " " + r.getLadoMaior() + " x " + r.getLadoMenor() + " - a área é " + r.getArea() + " e o perímetro é " + r.getPerimetro());
        Retangulo r2 = new Retangulo(5.7, 5.7);
        System.out.println(r2.getNome() + " " + r2.getLadoMaior() + " x " + r2.getLadoMenor() + " - a área é " + r2.getArea() + " e o perímetro é " + r2.getPerimetro());
        Quadrado q = new Quadrado(5.7);
        System.out.println(q.getNome() + " " + q.getLadoMaior() + " x " + q.getLadoMenor() + " - a área é " + q.getArea() + " e o perímetro é " + q.getPerimetro());
        TrianguloEquilatero te = new TrianguloEquilatero(5.7);
        System.out.println(te.getNome() + " lados " + te.getBase() + " - a área é " + te.getArea() + " e o perímetro é " + te.getPerimetro());
    }
}
